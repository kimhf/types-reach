// Type definitions for Reach

import * as firebasetypes from 'firebase/app'

export = reach;
export as namespace reach;

declare namespace reach {
    namespace firestore {
        namespace cause {
            export interface Cause {
                cid: string
                cover: reach.firestore.image.Meta | null
                description: string
                image: string
                orgRef: firebasetypes.firestore.DocumentReference | null
                slug: string
                title: string
                unitPrice: number
            }

            export interface Causes {
                [key: string]: Cause
            }
        }

        namespace image {
            export interface Detections {
                adult: string
                medical: string
                spoof: string
                violence: string
            }

            export interface Size {
                path: string
                name: string
                bucket: string
                downloadUrl: string
                contentType: string
            }

            export interface Sizes {
                [index: string]: reach.firestore.image.Size
            }

            export interface Meta {
                source: reach.firestore.image.Size | null
                detections: Detections | null
                censored: boolean
                sizes: reach.firestore.image.Sizes
            }
        }

        namespace user {
            export interface PublicProfile {
                displayName: string // The user's display name (if available).
                cover: reach.firestore.image.Meta | null // The user's cover ImageMeta (if available).
                photo: reach.firestore.image.Meta | null // The user's photo ImageMeta (if available).
                photoURL: string // The user's photoURL (if available). Can be third party source. If photo is set this url should corespond with that ImageMeta
                uid: string
            }

            export interface PrivateProfile {
                recoinWallet: string
                recoinAccount: string
            }
        }

        namespace org {
            export interface Org {
                cover: reach.firestore.image.Meta | null // The org's cover ImageMeta (if available).
                description: string
                id: string // The org's id. Thould be the same as its key.
                imageURL: string
                name: string
                slug: string // Url friendly representation of the org name.
                url: string
            }


            export interface Orgs {
                [key: string]: Org
            }
        }

        namespace event {
            export interface Event {
                addedByUID: string
                addedByUser: string
                category: string
                country: string
                countryCode: string
                cover: reach.firestore.image.Meta | null
                createdTimestamp: Date
                description: string
                fromDate: string
                fromTimestamp: Date | null
                geoHash: string
                geoPoint: firebasetypes.firestore.GeoPoint | null
                latitude: string
                longitude: string
                name: string
                state: string
                toDate: string
                toTimestamp: Date | null
                url: string
            }
        }

        namespace recoin {
            export interface Wallet {
                balance: number
                lastUpdateTimestamp: Date
                ownerId: string
                ownerType: "user" | "cause"
            }

            export interface Transaction {
                from: string
                to: string
                timestamp: Date
                description: string
                amount: number
                transactionId: string
            }
        }
    }

    namespace database {
        namespace cause {
            export interface Cause {
                cause: string
                causeImage: string
                cid: string
                compositeName: string
                cover: reach.database.image.Meta | null
                url: string
                description: string
                name: string
                org: string
                orgImage: string
                slug: string
                unitPrice: number
            }

            export interface Causes {
                [key: string]: Cause
            }
        }

        namespace image {
            export interface Size {
                bucket: string
                crop: boolean
                height: number
                name: string
                path: string
                sufix: string
                width: number
                downloadUrl: string
            }

            export interface Sizes {
                [key: string]: reach.database.image.Size
            }

            export interface Detections {
                adult: string
                medical: string
                spoof: string
                violence: string
            }

            export interface Meta {
                censored: boolean
                contentType: string
                detections: Detections | null
                originalBucket: string
                originalName: string
                originalPath: string
                sizes: reach.database.image.Sizes
                sizesBucket: string
                [key: string]: any
            }
        }

        namespace event {
            export interface Event {
                addedByUID: string
                addedByUser: string
                category: string
                country: string
                description: string
                fromDate: string
                latitude: string
                longitude: string
                name: string
                radius: string
                state: string
                toDate: string
                url: string
                cover?: reach.database.image.Meta
            }

            export interface Events {
                [key: string]: reach.database.event.Event
            }
        }

        namespace user {
            export interface Friend {
                imgURL: string
                nick: string
            }

            export interface FriendsList {
                [key: string]: Friend
            }

            export interface Profile {
                cover: reach.database.image.Meta | null
                htags: string
                latitude: string
                longitude: string
                mapID: any
                nick: string
                profImage: string
                reachLive: any
                recoins: any
                status: string
            }
        }

        namespace recoin {
            export interface Account {
                balance: number
                lastUpdateTimestamp: number
            }

            export interface AccountUserMeta {
                dailyLimitAmount: number
                dailyLimitDate: string
                bonusDayNum: number
                bonusDayDate: string
                recommendationWidgetViewNum: number
                recommendationWidgetViewTimestamp: number
                rewardedAerVideoViewTimestamp: number
                rewardedAerVideoViewNum: number
            }

            export interface Transaction {
                from: string
                to: string
                timestamp: number
                description: string
                amount: number
                transactionId: string
                task?: reach.server.Task
                meta?: any
            }
        }
    }

    namespace app {
        export interface Notifications {
            [key: string]: Notification
        }

        export interface Notification {
            message: string
            id: string
        }
    }

    namespace server {
        export interface Task {
            type: string
            uid: string
            tid: string
            [propName: string]: any
        }
    }

    namespace campaign {
        export interface Campaign {
            backersCount: number
            content: string
            causeId: string
            createdAt: Date
            creatorUid: string
            currency: string
            excerpt: string
            goal: number
            id: string
            launchedAt: Date | null
            recoinAccount: firebasetypes.firestore.DocumentReference | null
            slug: string
            status: string
            title: string
            updatedAt: Date
            pledged: number
        }

        export interface Campaigns {
            [key: string]: Campaign
        }

        export interface CampaignSubmitData {
            content: string
            causeId: string
            creatorUid?: string
            excerpt: string
            goal: number
            status?: string
            title: string
            updatedAt: firebasetypes.firestore.FieldValue
        }
    }
}
